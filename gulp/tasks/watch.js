var gulp = require('gulp'),
watch = require('gulp-watch'),
browserSync = require('browser-sync').create();

gulp.task('watch', function() {

  browserSync.init({
    notify: false,
    server: {
      baseDir: "src"
    }
  });

  watch(['./src/index.html', './src/partials/**/*.css', './src/styles.css', './src/app.js'], function(){
      browserSync.reload();
   });

  watch(['./src/partial/**/*.scss', './src/styles.scss'], function() {
    gulp.start('cssInject');
  });

});

gulp.task('cssInject', ['styles'], function() {
  return gulp.src('./src/styles.css')
    .pipe(browserSync.stream());
});