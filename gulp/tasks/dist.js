var gulp = require('gulp'),
    concat = require('gulp-concat');

// Run: gulp scripts
gulp.task('scripts', function() {
    var scripts = [
        './src/_js/vue.min.js',
        './src/_js/jquery.min.js',
        './src/_js/app-min.js'
    ];
    gulp.src(scripts)
        .pipe(concat('index.min.js'))
        .pipe(gulp.dest('./src/js/'));
});

// Run gulp dist to copies the files to the /dist folder for distributon
gulp.task('dist', ['scripts'], function() {
    gulp.src('./src/css/*').pipe(gulp.dest('./dist/css'));
    gulp.src('./src/js/*').pipe(gulp.dest('./dist/js'));
    gulp.src('./src/images/*').pipe(gulp.dest('./dist/images'));
    gulp.src('./src/index.html').pipe(gulp.dest('./dist/'));
});