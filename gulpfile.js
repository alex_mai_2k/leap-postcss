/*
 *  This version of gulp file only taking care of browserSync reload, js files combine into single file, and production distribution.
 *
 *  1. 'gulp watch' for development
 *  2. 'gulp scripts' to concat all js files into one
 *  3. 'gulp dist' to copy files for distribution
 */
var gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssvars = require('postcss-simple-vars'),
    nested = require('postcss-nested'),
    cssImport = require('postcss-import'),
    mixins = require('postcss-mixins'),
    rename = require("gulp-rename"),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat');

// require('./gulp/tasks/styles');
// require('./gulp/tasks/watch');
// require('./gulp/tasks/dist');


gulp.task('styles', function() {
    return gulp.src('./src/styles.css')
        .pipe(postcss([cssImport, mixins, cssvars, nested, autoprefixer]))
        .on('error', function(errorInfo) {
            console.log(errorInfo.toString());
            this.emit('end');
        })
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('watch', function() {

    browserSync.init({
        notify: false,
        server: {
            baseDir: "src"
        }
    });

    watch(['./src/index.html', './src/partials/**/*.css', './src/styles.css', './src/app.js'], function(){
        browserSync.reload();
    });

    watch(['./src/partial/**/*.scss', './src/styles.scss'], function() {
        gulp.start('cssInject');
    });

});

gulp.task('cssInject', ['styles'], function() {
    return gulp.src('./src/styles.css')
        .pipe(browserSync.stream());
});


// Run: gulp scripts
gulp.task('scripts', function() {
    var scripts = [
        './src/_js/vue.min.js',
        './src/_js/jquery.min.js',
        './src/_js/app-min.js'
    ];
    gulp.src(scripts)
        .pipe(concat('index.min.js'))
        .pipe(gulp.dest('./src/js/'));
});

// Run gulp dist to copies the files to the /dist folder for distributon
gulp.task('dist', ['scripts'], function() {
    gulp.src('./src/css/*').pipe(gulp.dest('./dist/css'));
    gulp.src('./src/js/*').pipe(gulp.dest('./dist/js'));
    gulp.src('./src/images/*').pipe(gulp.dest('./dist/images'));
    gulp.src('./src/index.html').pipe(gulp.dest('./dist/'));
});

